package com.example.wilmermendoza.servicesystem.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.wilmermendoza.servicesystem.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AgregarCliente extends Fragment {


    public AgregarCliente() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_agregar_cliente, container, false);


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Button registrar = (Button) getActivity().findViewById(R.id.botonRegCliente);
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ClienteFragment fragment = new ClienteFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fagment_content,fragment);
                fragmentTransaction.commit();
            }
        });

        TextView cancelar = (TextView) getActivity().findViewById(R.id.cancelarRegCliente);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClienteFragment fragment = new ClienteFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fagment_content,fragment);
                fragmentTransaction.commit();
            }
        });

    }
}
