package com.example.wilmermendoza.servicesystem;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.wilmermendoza.servicesystem.R;
import com.example.wilmermendoza.servicesystem.fragments.MecanicoFragment;

import org.w3c.dom.Text;

public class ModificarMecanico extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_mecanico);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar3);
        setSupportActionBar(toolbar);
        EditText cedula = (EditText) findViewById(R.id.txt_cedula_modmecanico);
        EditText nombre = (EditText) findViewById(R.id.txt_nombre_modmecanico);
        EditText direccion = (EditText) findViewById(R.id.txt_direccion_modmecanico);
        EditText telefono = (EditText) findViewById(R.id.txt_telefono_modmecanico);

        cedula.setText(getIntent().getStringExtra("cedula").toString());
        nombre.setText(getIntent().getStringExtra("nombre").toString());
        direccion.setText(getIntent().getStringExtra("direccion").toString());
        telefono.setText(getIntent().getStringExtra("telefono").toString());

    }

}
