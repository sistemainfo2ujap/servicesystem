package com.example.wilmermendoza.servicesystem;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class ServicioModificar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicio_modificar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);

        EditText txt_codigo = (EditText) findViewById(R.id.codigo_modserv);
        EditText txt_descripcion = (EditText) findViewById(R.id.descripcion_modserv);
        EditText txt_costo = (EditText) findViewById(R.id.costo_modserv);
        String codigo = getIntent().getStringExtra("codigo");
        String descripcion = getIntent().getStringExtra("descripcion");
        String costo = getIntent().getStringExtra("costo");

        txt_codigo.setText(codigo);
        txt_descripcion.setText(descripcion);
        txt_costo.setText(costo);
    }

}
