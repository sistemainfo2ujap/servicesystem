package com.example.wilmermendoza.servicesystem.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.wilmermendoza.servicesystem.R;
import com.example.wilmermendoza.servicesystem.adapters.ServiciosAdapter;
import com.example.wilmermendoza.servicesystem.models.Servicio;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class ServiciosFragment extends Fragment {

    ArrayList<Servicio> dataset ;

    public ServiciosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_servicios, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //----------- URLS del web service --------/////
        String URL = "http://frenosmonsterprueba.esy.es/public/obtener_servicios";

        //---------------------------------------------////
    //----------------------- parte donde me conecto con el web service -------------//
        RequestQueue queue = Volley.newRequestQueue(getActivity());
        //se crea un dialog mientras carga el web service
        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Espere Por Favor", "estamos atendiendo su solicitud");
        JsonArrayRequest request2 = new JsonArrayRequest(URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                dataset = new ArrayList<Servicio>();
                dataset = parser(response);
                Log.e("mirespuesta",response.toString());
                RecyclerView recyclerView = (RecyclerView) getActivity().findViewById(R.id.servicio_recyclerview);
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(new ServiciosAdapter(dataset, R.layout.row_servicios));
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                progressDialog.cancel();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"Error", Toast.LENGTH_SHORT).show();
                Log.e("error_volley",error.toString());
                progressDialog.cancel();
            }
        });

             queue.add(request2);

        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.flt_servicio);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServiciosAgregarFragment fragment = new ServiciosAgregarFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fagment_content,fragment);
                fragmentTransaction.commit();
            }
        });
    }
    public ArrayList<Servicio> parser (JSONArray response) {
        // funcion que te modifica el adapter con los datos que vienen del web serive

        ArrayList<Servicio> ServAux = new ArrayList<Servicio>();
        for (int i = 0 ; i<response.length();i++){
            Servicio servicio = new Servicio();
            try {
                JSONObject jsonObject = (JSONObject) response.get(i);
                servicio.setCodigo(jsonObject.getString("codigo"));
                servicio.setDescripcion(jsonObject.getString("descripcion"));
                servicio.setPrecio(Float.parseFloat(jsonObject.getString("costo")));
                ServAux.add(servicio);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
      return ServAux;
    }
}
