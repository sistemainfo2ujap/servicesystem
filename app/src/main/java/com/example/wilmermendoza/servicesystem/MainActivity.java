package com.example.wilmermendoza.servicesystem;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wilmermendoza.servicesystem.fragments.ClienteFragment;
import com.example.wilmermendoza.servicesystem.fragments.HistorialFragment;
import com.example.wilmermendoza.servicesystem.fragments.MecanicoFragment;
import com.example.wilmermendoza.servicesystem.fragments.ServiciosAgregarFragment;
import com.example.wilmermendoza.servicesystem.fragments.ServiciosFragment;
import com.example.wilmermendoza.servicesystem.models.Servicio;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Toolbar toolbar =null;
    NavigationView navigationView =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //---------------------- Sustitucion de fragment por fragment----------------------//
        HistorialFragment fragment = new HistorialFragment();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fagment_content,fragment);
        fragmentTransaction.commit();
        //-----------------------------------------------------------------------------

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        //en estos if wilmer se coloca la accion de los botones del menu para que sepas por si a las mosks !!!!!!
        if (id == R.id.nav_historial) {

            HistorialFragment fragment = new HistorialFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fagment_content,fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_servicio) {

            ServiciosFragment fragment = new ServiciosFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fagment_content,fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_cliente) {
            ClienteFragment fragment = new ClienteFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fagment_content,fragment);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_mecanico) {
            MecanicoFragment fragment = new MecanicoFragment();
            android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fagment_content,fragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_logout) {
            Intent i =new Intent(this,Login.class);
            startActivity(i);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    protected void onPause(){
        super.onPause();
    }
}
