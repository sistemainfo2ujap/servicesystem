package com.example.wilmermendoza.servicesystem.models;

/**
 * Created by luis on 08/03/16.
 */
public class Servicio {

    private String descripcion;
    private String codigo;
    private float precio;


    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

}
