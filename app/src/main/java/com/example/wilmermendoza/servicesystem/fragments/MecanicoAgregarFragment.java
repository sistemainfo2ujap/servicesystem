package com.example.wilmermendoza.servicesystem.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.wilmermendoza.servicesystem.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class MecanicoAgregarFragment extends Fragment {


    public MecanicoAgregarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mecanico_agregar, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final EditText txt_cedula = (EditText) getActivity().findViewById(R.id.txt_cedula_mecanico);
        final EditText txt_nombre = (EditText) getActivity().findViewById(R.id.txt_nombre_mecanico);
        final EditText txt_direccion = (EditText) getActivity().findViewById(R.id.txt_direcciion_mecanico);
        final EditText txt_telefono = (EditText) getActivity().findViewById(R.id.txt_telefono_mecanico);

        TextView cancelar = (TextView) getActivity().findViewById(R.id.cancelar_mecanico);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MecanicoFragment fragment = new MecanicoFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fagment_content,fragment);
                fragmentTransaction.commit();
            }
        });

        Button agregar = (Button) getActivity().findViewById(R.id.agregar_mecanico);
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestQueue queue = Volley.newRequestQueue(getActivity());
                String URL = "http://frenosmonsterprueba.esy.es/public/insertar_mecanicos";
                String cedula = txt_cedula.getText().toString();
                String nombre = txt_nombre.getText().toString();
                String direccion = txt_direccion.getText().toString();
                String telefono = txt_telefono.getText().toString();
                final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Espere Por Favor", "estamos atendiendo su solicitud");
                try {
                    final JSONObject jsonObject = new JSONObject("{\"cedula\":\""+cedula+"\",\"descripcion\":\""+nombre+"\",\"direccion\":\""+direccion+"\",\"telefono\":\""+telefono+"\"}");
                    Log.e("objeto",jsonObject.toString());
                    JsonObjectRequest  request = new JsonObjectRequest(URL, jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            txt_cedula.setText("");
                            txt_direccion.setText("");
                            txt_nombre.setText("");
                            txt_telefono.setText("");
                            progressDialog.cancel();
                            Toast.makeText(getActivity(), "Operacion Exitosa", Toast.LENGTH_SHORT).show();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                            Log.e("error", error.toString());
                            progressDialog.cancel();
                        }
                    });
                    queue.add(request);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
