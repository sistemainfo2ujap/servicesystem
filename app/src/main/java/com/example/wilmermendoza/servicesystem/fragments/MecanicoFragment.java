package com.example.wilmermendoza.servicesystem.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.wilmermendoza.servicesystem.R;
import com.example.wilmermendoza.servicesystem.adapters.ClienteAdapter;
import com.example.wilmermendoza.servicesystem.adapters.MecanicoAdapter;
import com.example.wilmermendoza.servicesystem.models.Mecanico;
import com.example.wilmermendoza.servicesystem.models.Servicio;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MecanicoFragment extends Fragment {

    ArrayList<Mecanico> dataset;
    public MecanicoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mecanico, container, false);

        FloatingActionButton floatingActionButton = (FloatingActionButton) view.findViewById(R.id.flt_mecanico);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MecanicoAgregarFragment fragment = new MecanicoAgregarFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fagment_content,fragment);
                fragmentTransaction.commit();
            }
        });
        return  view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

       //--------------- URL del web service ----------///
        String URL = "http://frenosmonsterprueba.esy.es/public/obtener_mecanicos"; // url de donde va a jalar el json

        RequestQueue queue = Volley.newRequestQueue(getActivity()); // esto es una cola donde se guarda la respuesta
        final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Espere Por Favor", "estamos atendiendo su solicitud");
        JsonArrayRequest request = new JsonArrayRequest(URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) { // respuesta bn !!
                dataset = new ArrayList<Mecanico>();
                dataset=parser(response);
                RecyclerView recyclerView = (RecyclerView) getActivity().findViewById(R.id.mecanico_recyclerview);
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(new MecanicoAdapter(dataset, R.layout.row_mecanico));
                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                progressDialog.cancel();
            }
        }, new Response.ErrorListener() { // cuando hay algun error
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),"Error", Toast.LENGTH_SHORT).show();
                Log.e("error_volley", error.toString());
                progressDialog.cancel();
            }
        });

        queue.add(request); // aqui se agrega la respuesta del json a la cola

    }
    public ArrayList<Mecanico> parser (JSONArray response){ //funcion para el adapter del card view

        ArrayList<Mecanico> MecanicoAux = new ArrayList<Mecanico>();
        for (int i = 0; i<response.length();i++){
            Mecanico mecanico = new Mecanico();
            try {
                JSONObject jsonObject = (JSONObject) response.get(i);
                mecanico.setCedula(jsonObject.getString("cedula"));
                mecanico.setNombre_apellido(jsonObject.getString("descripcion"));
                mecanico.setDireccion(jsonObject.getString("direccion"));
                mecanico.setTelefono(jsonObject.getString("telefono"));

                MecanicoAux.add(mecanico);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return MecanicoAux;
    }
}
