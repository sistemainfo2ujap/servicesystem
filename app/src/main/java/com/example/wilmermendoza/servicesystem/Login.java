package com.example.wilmermendoza.servicesystem;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {
    EditText email,cont;
    String mail,contra;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = (EditText) findViewById(R.id.emailLogin);
        cont = (EditText) findViewById(R.id.contLog);


    }

    public void irMain(View v) throws JSONException {
        RequestQueue queue = Volley.newRequestQueue(this);
        String URL = "http://frenosmonsterprueba.esy.es/public/login_android";
        mail = email.getText().toString();
        contra = cont.getText().toString();
        final Intent intent = new Intent(this,MainActivity.class);

        if (mail.equals("") || contra.equals("")){
            Toast toast = Toast.makeText(getApplicationContext(),"Por favor rellene todos los campos...", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER| Gravity.CENTER,0,0);
            toast.show();
        }

        else {
            final ProgressDialog progressDialog = ProgressDialog.show(this, "Espere Por Favor", "estamos atendiendo su solicitud");
            try{
                JSONObject jsonObject = new JSONObject("{\"name\":\""+mail+"\",\"password\":\""+contra+"\"}");
                JsonObjectRequest request = new JsonObjectRequest(URL, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("estado").equals("true")){
                                Toast.makeText(Login.this, "Ingresando", Toast.LENGTH_SHORT).show();
                                startActivity(intent);
                                progressDialog.cancel();
                            }else{
                                Toast.makeText(Login.this, "Usuario o Contraseñas incorrectos", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Login.this, "ERROR", Toast.LENGTH_SHORT).show();
                    }
                });
            queue.add(request);
            }catch (JSONException e){

            }

        }

    }

    public void irReg(View v){Intent i = new Intent(this, RegistroUsuario.class);
        startActivity(i);
    }

    protected void onPause(){
        super.onPause();
        finish();
    }



}
