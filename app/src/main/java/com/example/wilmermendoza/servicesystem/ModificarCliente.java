package com.example.wilmermendoza.servicesystem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ModificarCliente extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar_cliente);

        EditText nombre = (EditText) findViewById(R.id.nombreModCliente);
        EditText cedula = (EditText) findViewById(R.id.cedulaModCliente);
        EditText direccion = (EditText) findViewById(R.id.direccionModCliente);
        EditText telefono = (EditText) findViewById(R.id.telefonoModCliente);
        EditText email = (EditText) findViewById(R.id.emailModcliente);

        nombre.setText(getIntent().getStringExtra("nombre").toString());
        cedula.setText(getIntent().getStringExtra("cedula").toString());
        direccion.setText(getIntent().getStringExtra("direccion").toString());
        telefono.setText(getIntent().getStringExtra("telefono").toString());
        email.setText(getIntent().getStringExtra("email").toString());



    }

    @Override
    protected void onPause() {
        super.onPause();
        this.finish();
    }


}
