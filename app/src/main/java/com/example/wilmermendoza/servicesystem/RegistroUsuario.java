package com.example.wilmermendoza.servicesystem;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class RegistroUsuario extends AppCompatActivity {
    EditText nombre;
    EditText apellido ;
    EditText email ;
    EditText telefono ;
    EditText contraseña;

    String nomb,apell,mail,telf,cont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);


        nombre = (EditText) findViewById(R.id.nombreReg);
        apellido = (EditText) findViewById(R.id.apellidoReg);
        email = (EditText) findViewById(R.id.emailReg);
        telefono = (EditText) findViewById(R.id.numeroReg);
        contraseña = (EditText) findViewById(R.id.contReg);

    }

    public void irMain2(View v){


        nomb = nombre.getText().toString();
        apell = apellido.getText().toString();
        mail = email.getText().toString();
        telf = telefono.getText().toString();
        cont = contraseña.getText().toString();



        if ((nomb.equals("")) || (apell.equals(""))  || (mail.equals("")) || (telf.equals("")) || (cont.equals("")) ){
            Toast toast = Toast.makeText(getApplicationContext(),"Por favor rellene todos los campos...", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER| Gravity.CENTER,0,0);
            toast.show();
        }


        else {
            String URL="http://frenosmonsterprueba.esy.es/public/registrar_android";
            final RequestQueue queue = Volley.newRequestQueue(this);
            final Intent i = new Intent(this, Login.class);
            final ProgressDialog progressDialog = ProgressDialog.show(this, "Espere Por Favor", "estamos atendiendo su solicitud");
            try{
                final JSONObject jsonObject = new JSONObject("{\"name\":\""+nomb+" "+apell+"\",\"password\":\""+cont+"\",\"email\":\""+mail+"\",\"phone\":\""+telf+"\"}");
                final JsonObjectRequest request = new JsonObjectRequest(URL, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            progressDialog.cancel();
                            Log.e("mensaje",jsonObject.toString());
                            Toast.makeText(RegistroUsuario.this,response.getString("estado"), Toast.LENGTH_SHORT).show();
                            startActivity(i);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.cancel();
                        Toast.makeText(RegistroUsuario.this,error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

                queue.add(request);

            }catch (JSONException e){

            }

        }

    }

        public void irLogin(View view){
            Intent i = new Intent(this, Login.class);
            startActivity(i);

        }

    protected void onPause(){
        super.onPause();
        finish();
    }

}
