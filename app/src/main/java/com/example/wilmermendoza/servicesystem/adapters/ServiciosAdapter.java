package com.example.wilmermendoza.servicesystem.adapters;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wilmermendoza.servicesystem.MainActivity;
import com.example.wilmermendoza.servicesystem.R;
import com.example.wilmermendoza.servicesystem.ServicioModificar;
import com.example.wilmermendoza.servicesystem.models.Servicio;

import java.util.ArrayList;

/**
 * Created by luis on 09/03/16.
 */
public class ServiciosAdapter extends RecyclerView.Adapter<ServiciosAdapter.ViewHolder>{

    private ArrayList<Servicio> servicios;
    private int itemlayout;


    public ServiciosAdapter (ArrayList<Servicio> servicios,int itemlayout){
        this.servicios = servicios;
        this.itemlayout= itemlayout;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemlayout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Servicio servicio = servicios.get(position);
        holder.servicioCodigo.setText(servicio.getCodigo());
        holder.servicioDescripcion.setText(servicio.getDescripcion());
        holder.servicioPrecio.setText(String.valueOf(servicio.getPrecio()));

    }

    @Override
    public int getItemCount() {
        if(servicios!= null){
        return servicios.size();
        }
        return 0;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
       public TextView servicioCodigo;
       public TextView servicioDescripcion;
       public TextView servicioPrecio;

       public ViewHolder(final View itemView) {
           super(itemView);

           servicioCodigo = (TextView) itemView.findViewById(R.id.servicio_codigo);
           servicioDescripcion = (TextView) itemView.findViewById(R.id.servicio_descripcion);
           servicioPrecio = (TextView) itemView.findViewById(R.id.servicio_precio);
           itemView.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Intent i = new Intent(v.getContext(),ServicioModificar.class);
                   i.putExtra("codigo",servicioCodigo.getText());
                   i.putExtra("descripcion",servicioDescripcion.getText());
                   i.putExtra("costo",servicioPrecio.getText());
                   v.getContext().startActivity(i);
               }
           });

       }

   }

}
