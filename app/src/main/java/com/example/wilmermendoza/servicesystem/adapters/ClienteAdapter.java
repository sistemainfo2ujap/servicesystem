package com.example.wilmermendoza.servicesystem.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.wilmermendoza.servicesystem.ModificarCliente;
import com.example.wilmermendoza.servicesystem.ModificarMecanico;
import com.example.wilmermendoza.servicesystem.R;
import com.example.wilmermendoza.servicesystem.models.Cliente;

import java.util.ArrayList;

/**
 * Created by wilmermendoza on 13/3/16.
 */
public class ClienteAdapter extends RecyclerView.Adapter<ClienteAdapter.ViewHolder> {

    private ArrayList<Cliente> clientes;
    private int itemLayout;

    public ClienteAdapter(ArrayList<Cliente> clientes, int itemLayout) {
        this.clientes = clientes;
        this.itemLayout = itemLayout;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(itemLayout,viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        Cliente cliente = clientes.get(position);

        viewHolder.nombreCliente.setText(cliente.getNombreCliente());
        viewHolder.cedulaCliente.setText(cliente.getCedulaCliente());
        viewHolder.emailCliente.setText(cliente.getEmail());
        viewHolder.direccionCliente.setText(cliente.getDireccion());
        viewHolder.telefonoCliente.setText(cliente.getTelefono());
    }

    @Override
    public int getItemCount() {
        if(clientes!= null){
            return clientes.size();
        }
        return 0;    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView nombreCliente ;
        public TextView cedulaCliente;
        public TextView emailCliente;
        public TextView direccionCliente;
        public TextView telefonoCliente;


        public ViewHolder(View itemView) {
            super(itemView);

            nombreCliente = (TextView) itemView.findViewById(R.id.rowNombreC);
            cedulaCliente = (TextView) itemView.findViewById(R.id.rowCedulaC);
            emailCliente = (TextView) itemView.findViewById(R.id.rowEmailC);
            telefonoCliente  = (TextView) itemView.findViewById(R.id.rowTelefonoC);
            direccionCliente = (TextView) itemView.findViewById(R.id.rowDirC);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), ModificarCliente.class);
                    intent.putExtra("nombre",nombreCliente.getText());
                    intent.putExtra("cedula",cedulaCliente.getText());
                    intent.putExtra("email",emailCliente.getText());
                    intent.putExtra("telefono",telefonoCliente.getText());
                    intent.putExtra("direccion",direccionCliente.getText());
                    view.getContext().startActivity(intent);
                }
            });

        }
    }




}
