package com.example.wilmermendoza.servicesystem.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.wilmermendoza.servicesystem.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiciosAgregarFragment extends Fragment {


    public ServiciosAgregarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_servicios_agregar, container, false);
        return  rootview;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final EditText txt_nombre = (EditText) getActivity().findViewById(R.id.txt_codigo_addserv);
        final EditText txt_direccion = (EditText) getActivity().findViewById(R.id.txt_servicio_addserv);
              final EditText txt_costo = (EditText) getActivity().findViewById(R.id.txt_precio_addserv);
        Button cancelar = (Button) getActivity().findViewById(R.id.cancelar_addserv);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServiciosFragment fragment = new ServiciosFragment();
                android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fagment_content,fragment);
                fragmentTransaction.commit();
            }
        });

        Button agregar = (Button) getActivity().findViewById(R.id.guardar_addserv);
        agregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestQueue queue = Volley.newRequestQueue(getActivity());
                String URL = "http://frenosmonsterprueba.esy.es/public/insertar_servicios";
                final String nombre;
                final String direccion;
                String costo;
                nombre = txt_nombre.getText().toString();
                direccion = txt_direccion.getText().toString();
                costo = txt_costo.getText().toString();
               // Log.e("nombre",nombre);
               // Log.e("direccion",direccion);

                final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Espere Por Favor", "estamos atendiendo su solicitud");
                try {
                    final  JSONObject jsonObject = new JSONObject("{\"codigo\":\""+nombre+"\",\"descripcion\":\""+direccion+"\",\"costo\":\""+costo+"\"}");
                   JsonObjectRequest jsonObjectRequestnew = new JsonObjectRequest(URL, jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            txt_direccion.setText("");
                            txt_nombre.setText("");
                            txt_costo.setText("");
                            Toast.makeText(getActivity(),"Operacion Exitosa", Toast.LENGTH_SHORT).show();
                            progressDialog.cancel();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("respuesta mala",error.toString());
                            progressDialog.cancel();
                        }
                    });

                    queue.add(jsonObjectRequestnew);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
