package com.example.wilmermendoza.servicesystem.adapters;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.wilmermendoza.servicesystem.MainActivity;
import com.example.wilmermendoza.servicesystem.ModificarMecanico;
import com.example.wilmermendoza.servicesystem.R;
import com.example.wilmermendoza.servicesystem.ServicioModificar;
import com.example.wilmermendoza.servicesystem.fragments.HistorialFragment;
import com.example.wilmermendoza.servicesystem.models.Mecanico;
import com.example.wilmermendoza.servicesystem.models.Servicio;

import java.util.ArrayList;

/**
 * Created by luis on 16/03/16.
 */
public class MecanicoAdapter extends RecyclerView.Adapter<MecanicoAdapter.ViewHolder> {

    private ArrayList<Mecanico> mecanicos;
    private int itemlayout;

    public MecanicoAdapter (ArrayList<Mecanico> mecanicos,int itemlayout){
        this.mecanicos = mecanicos;
        this.itemlayout= itemlayout;
    }

    @Override
    public MecanicoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(itemlayout,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MecanicoAdapter.ViewHolder holder, int position) {
        Mecanico mecanico = mecanicos.get(position);
        holder.mecanicoCedula.setText(mecanico.getCedula());
        holder.mecanicoNombre_Apellido.setText(mecanico.getNombre_apellido());
        holder.mecanicoTelefono.setText(mecanico.getTelefono());
        holder.mecanicoDireccion.setText(mecanico.getDireccion());
    }

    @Override
    public int getItemCount() {
        if(mecanicos!= null){
            return mecanicos.size();
        }
        return 0;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mecanicoCedula;
        public TextView mecanicoNombre_Apellido;
        public TextView mecanicoDireccion;
        public TextView mecanicoTelefono;

        public ViewHolder(final View itemView) {
            super(itemView);
            mecanicoCedula = (TextView) itemView.findViewById(R.id.cedula_mecanico);
            mecanicoNombre_Apellido = (TextView) itemView.findViewById(R.id.nombre_mecanico);
            mecanicoDireccion = (TextView) itemView.findViewById(R.id.direccion_mecanico);
            mecanicoTelefono = (TextView) itemView.findViewById(R.id.telf_mecanico);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent  intent = new Intent(v.getContext(), ModificarMecanico.class);
                    intent.putExtra("cedula",mecanicoCedula.getText());
                    intent.putExtra("nombre",mecanicoNombre_Apellido.getText());
                    intent.putExtra("direccion",mecanicoDireccion.getText());
                    intent.putExtra("telefono",mecanicoTelefono.getText());
                    v.getContext().startActivity(intent);
                }
            });
        }
    }
}
