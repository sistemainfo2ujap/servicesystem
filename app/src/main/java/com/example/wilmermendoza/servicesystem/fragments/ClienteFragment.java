package com.example.wilmermendoza.servicesystem.fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.wilmermendoza.servicesystem.R;
import com.example.wilmermendoza.servicesystem.adapters.ClienteAdapter;
import com.example.wilmermendoza.servicesystem.adapters.MecanicoAdapter;
import com.example.wilmermendoza.servicesystem.models.Cliente;
import com.example.wilmermendoza.servicesystem.models.Mecanico;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */

    public class ClienteFragment extends Fragment {

    ArrayList<Cliente> dataset;


    public ClienteFragment() {
            // Required empty public constructor
        }


        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootview = inflater.inflate(R.layout.fragment_cliente, container, false);

            FloatingActionButton fab = (FloatingActionButton) rootview.findViewById(R.id.fabCliente);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AgregarCliente fragment = new AgregarCliente();
                    android.support.v4.app.FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fagment_content,fragment);
                    fragmentTransaction.commit();
                }
            });
            // Inflate the layout for this fragment
            return rootview;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
//--------------- URL del web service ----------///
            String URL = "http://frenosmonsterprueba.esy.es/public/obtener_clientes"; // url de donde va a jalar el json

            RequestQueue queue = Volley.newRequestQueue(getActivity()); // esto es una cola donde se guarda la respuesta
            final ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "Espere Por Favor", "estamos atendiendo su solicitud");
            JsonArrayRequest request = new JsonArrayRequest(URL, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) { // respuesta bn !!
                    dataset = new ArrayList<Cliente>();
                    dataset = parser(response);
                    RecyclerView recyclerView = (RecyclerView) getActivity().findViewById(R.id.recyclerviewCliente);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setAdapter(new ClienteAdapter(dataset, R.layout.row_clientes));
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    progressDialog.cancel();
                }
            }, new Response.ErrorListener() { // cuando hay algun error
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                    Log.e("error_volley", error.toString());
                    progressDialog.cancel();
                }
            });

            queue.add(request); // aqui se agrega la respuesta del json a la cola

        }
    public ArrayList<Cliente> parser (JSONArray response){ //funcion para el adapter del card view

        ArrayList<Cliente> Clienteaux = new ArrayList<Cliente>();
        for (int i = 0; i<response.length();i++){
            Cliente cliente = new Cliente();
            try {
                JSONObject jsonObject = (JSONObject) response.get(i);
                cliente.setNombreCliente(jsonObject.getString("nombre"));
                cliente.setCedulaCliente(jsonObject.getString("cedula"));
                cliente.setDireccion(jsonObject.getString("direccion"));
                cliente.setTelefono(jsonObject.getString("telefono"));
                cliente.setEmail(jsonObject.getString("email"));

                Clienteaux.add(cliente);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return Clienteaux;
    }
    }
